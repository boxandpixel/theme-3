<?php 
/* 
Template Name: Home
*/
?>

<?php get_header(); ?>

<!-- RECODED -->

	<div class="row">
		<div class="small-12 medium-12 collapse end" role="banner">
		
		<?php while (have_posts()) : the_post(); ?>
			<article <?php post_class() ?> id="post-<?php the_ID(); ?>">
				<div class="entry-content">
					<?php the_content(); ?>
				</div>
			</article>
		<?php endwhile;?>

		</div> <!-- .small-12 medium-12 role=banner -->
	</div> <!-- .row -->



<div class="wide">
	<div class="row">
		<div class="small-12 medium-12 large-12 collapse" role="main">

		    <?php
		    	$args = array('post_type' => 'clients');  
		    	$loop = new WP_Query( $args );  
		    	while ( $loop->have_posts() ) : $loop->the_post(); $counter++; 

				$image = get_field('project_image_main');
				if( !empty($image) ): 
					$url = $image['url'];
					$alt = $image['alt'];
					$home_large = $image['sizes']['fp-small'];
					$home_small = $image['sizes']['home-images-small'];
				endif;

				$view = get_field('view_on_home_page');

				//if (!empty($view)):
			?>
			
		
			<?php if ($counter == 1): ?>
			<!-- Row 1 -->
			<div class="home-row">	

				<div class="home-row-big-left">
					<div class="counter1 project">
						<a href="<?php the_permalink(); ?>"><img src="<?php echo $home_large; ?>" alt="<?php echo $alt; ?>" class="preview"></a>
						<a href="<?php the_permalink(); ?>" class="project-hover">
							<span class="preview-center">
								<span class="hover-title"><?php the_title(); ?></span>
								<p><span class="hover-banner"><?php the_field('project_banner'); ?></span></p>
								<img src="/wp-content/themes/bfdg/images/overlay-arrow.svg" alt="View <?php the_title(); ?>">
							</span> <!-- .preview-center -->
						</a> <!-- .project-hover -->
					</div> <!-- .counter1 -->
				</div> <!-- .home-row-big-left -->
				
				
			<?php elseif ($counter == 2): ?>
				<div class="home-row-small-right">
					<div class="counter2 project">
						<a href="<?php the_permalink(); ?>"><img src="<?php echo $home_small; ?>" alt="<?php echo $alt; ?>" class="preview"></a>
						<a href="<?php the_permalink(); ?>" class="project-hover">
							<span class="preview-center">
								<span class="hover-title"><?php the_title(); ?></span>
								<p><span class="hover-banner"><?php the_field('project_banner'); ?></span></p>
								<img src="/wp-content/themes/bfdg/images/overlay-arrow.svg" alt="View <?php the_title(); ?>">
							</span> <!-- .preview-center -->
						</a> <!-- .project-hover -->
					</div> <!-- .counter2 -->							

			<?php elseif ($counter == 3): ?>
				
					<div class="counter3 project">
						<a href="<?php the_permalink(); ?>"><img src="<?php echo $home_small; ?>" alt="<?php echo $alt; ?>" class="preview"></a>
						<a href="<?php the_permalink(); ?>" class="project-hover">
							<span class="preview-center">
								<span class="hover-title"><?php the_title(); ?></span>
								<p><span class="hover-banner"><?php the_field('project_banner'); ?></span></p>
								<img src="/wp-content/themes/bfdg/images/overlay-arrow.svg" alt="View <?php the_title(); ?>">
							</span> <!-- .preview-center -->
						</a> <!-- .project-hover -->
					</div> <!-- .counter3 -->

				</div> <!-- .home-row-small-right -->		

			</div> <!-- .home-row row1 -->


			<?php elseif ($counter == 4): ?>
			<!-- Row 2 -->
			<div class="home-row">

				<div class="home-row-small-left">

					<div class="counter4 project">
						<a href="<?php the_permalink(); ?>"><img src="<?php echo $home_small; ?>" alt="<?php echo $alt; ?>" class="preview"></a>
						<a href="<?php the_permalink(); ?>" class="project-hover">
							<span class="preview-center">
								<span class="hover-title"><?php the_title(); ?></span>
								<p><span class="hover-banner"><?php the_field('project_banner'); ?></span></p>
								<img src="/wp-content/themes/bfdg/images/overlay-arrow.svg" alt="View <?php the_title(); ?>">
							</span> <!-- .preview-center -->
						</a> <!-- .project-hover -->
					</div> <!-- .counter4 -->	

			<?php elseif ($counter == 5): ?>
			
					<div class="counter5 project">
						<a href="<?php the_permalink(); ?>"><img src="<?php echo $home_small; ?>" alt="<?php echo $alt; ?>" class="preview"></a>
						<a href="<?php the_permalink(); ?>" class="project-hover">
							<span class="preview-center">
								<span class="hover-title"><?php the_title(); ?></span>
								<p><span class="hover-banner"><?php the_field('project_banner'); ?></span></p>
								<img src="/wp-content/themes/bfdg/images/overlay-arrow.svg" alt="View <?php the_title(); ?>">
							</span> <!-- .preview-center -->
						</a> <!-- .project-hover -->
					</div> <!-- .counter5 -->

				</div> <!-- .home-row-small-left -->

			<?php elseif ($counter == 6): ?>
				<div class="home-row-big-right">

					<div class="counter6 project">
						<a href="<?php the_permalink(); ?>"><img src="<?php echo $home_large; ?>" alt="<?php echo $alt; ?>" class="preview"></a>
						<a href="<?php the_permalink(); ?>" class="project-hover">
							<span class="preview-center">
								<span class="hover-title"><?php the_title(); ?></span>
								<p><span class="hover-banner"><?php the_field('project_banner'); ?></span></p>
								<img src="/wp-content/themes/bfdg/images/overlay-arrow.svg" alt="View <?php the_title(); ?>">
							</span> <!-- .preview-center -->
						</a> <!-- .project-hover -->
					</div> <!-- .counter6 -->	

				</div> <!-- home-row-big-right -->

			</div> <!-- .home-row -->

					
			<?php elseif ($counter == 7): ?>
			<!-- Row 3 -->
			<div class="home-row">

				<div class="home-row-big-left">

					<div class="counter7 project">
						<a href="<?php the_permalink(); ?>"><img src="<?php echo $home_large; ?>" alt="<?php echo $alt; ?>" class="preview"></a>
						<a href="<?php the_permalink(); ?>" class="project-hover">
							<span class="preview-center">
								<span class="hover-title"><?php the_title(); ?></span>
								<p><span class="hover-banner"><?php the_field('project_banner'); ?></span></p>
								<img src="/wp-content/themes/bfdg/images/overlay-arrow.svg" alt="View <?php the_title(); ?>">
							</span> <!-- .preview-center -->
						</a> <!-- .project-hover -->
					</div> <!-- .counter7 -->	

				</div> <!-- .home-row-big-left -->

			<?php elseif ($counter == 8): ?>
				<div class="home-row-small-right">

					<div class="counter8 project">
						<a href="<?php the_permalink(); ?>"><img src="<?php echo $home_small; ?>" alt="<?php echo $alt; ?>" class="preview"></a>
						<a href="<?php the_permalink(); ?>" class="project-hover">
							<span class="preview-center">
								<span class="hover-title"><?php the_title(); ?></span>
								<p><span class="hover-banner"><?php the_field('project_banner'); ?></span></p>
								<img src="/wp-content/themes/bfdg/images/overlay-arrow.svg" alt="View <?php the_title(); ?>">
							</span> <!-- .preview-center -->
						</a> <!-- .project-hover -->
					</div> <!-- .counter8 -->	


			<?php elseif ($counter == 9): ?>
			
					<div class="counter9 project">
						<a href="<?php the_permalink(); ?>"><img src="<?php echo $home_small; ?>" alt="<?php echo $alt; ?>" class="preview"></a>
						<a href="<?php the_permalink(); ?>" class="project-hover">
							<span class="preview-center">
								<span class="hover-title"><?php the_title(); ?></span>
								<p><span class="hover-banner"><?php the_field('project_banner'); ?></span></p>
								<img src="/wp-content/themes/bfdg/images/overlay-arrow.svg" alt="View <?php the_title(); ?>">
							</span> <!-- .preview-center -->
						</a> <!-- .project-hover -->
					</div> <!-- .counter9 -->	

				</div> <!-- .home-row-small-right -->

			</div> <!-- .home-row -->

			<?php elseif ($counter == 10): ?>
			<!-- Row 4 -->
			<div class="home-row">

				<div class="home-row-small-left">

					<div class="counter10 project">
						<a href="<?php the_permalink(); ?>"><img src="<?php echo $home_small; ?>" alt="<?php echo $alt; ?>" class="preview"></a>
						<a href="<?php the_permalink(); ?>" class="project-hover">
							<span class="preview-center">
								<span class="hover-title"><?php the_title(); ?></span>
								<p><span class="hover-banner"><?php the_field('project_banner'); ?></span></p>
								<img src="/wp-content/themes/bfdg/images/overlay-arrow.svg" alt="View <?php the_title(); ?>">
							</span> <!-- .preview-center -->
						</a> <!-- .project-hover -->
					</div> <!-- .counter10 -->	

			<?php elseif ($counter == 11): ?>
				
					<div class="counter11 project">
						<a href="<?php the_permalink(); ?>"><img src="<?php echo $home_small; ?>" alt="<?php echo $alt; ?>" class="preview"></a>
						<a href="<?php the_permalink(); ?>" class="project-hover">
							<span class="preview-center">
								<span class="hover-title"><?php the_title(); ?></span>
								<p><span class="hover-banner"><?php the_field('project_banner'); ?></span></p>
								<img src="/wp-content/themes/bfdg/images/overlay-arrow.svg" alt="View <?php the_title(); ?>">
							</span> <!-- .preview-center -->
						</a> <!-- .project-hover -->
					</div> <!-- .counter11 -->	

			</div> <!-- .home-row-small-left -->

			<?php elseif ($counter == 12): ?>
				<div class="home-row-big-right">

					<div class="counter12 project">
						<a href="<?php the_permalink(); ?>"><img src="<?php echo $home_large; ?>" alt="<?php echo $alt; ?>" class="preview"></a>
						<a href="<?php the_permalink(); ?>" class="project-hover">
							<span class="preview-center">
								<span class="hover-title"><?php the_title(); ?></span>
								<p><span class="hover-banner"><?php the_field('project_banner'); ?></span></p>
								<img src="/wp-content/themes/bfdg/images/overlay-arrow.svg" alt="View <?php the_title(); ?>">
							</span> <!-- .preview-center -->
						</a> <!-- .project-hover -->
					</div> <!-- .counter12 -->

				</div> <!-- .home-row-big-right -->

			</div> <!-- .home-row -->

			<?php elseif ($counter == 13): ?>
			<!-- Row 5 -->	
			<div class="home-row">

				<div class="home-row-big-left">

					<div class="counter13 project">
						<a href="<?php the_permalink(); ?>"><img src="<?php echo $home_large; ?>" alt="<?php echo $alt; ?>" class="preview"></a>
						<a href="<?php the_permalink(); ?>" class="project-hover">
							<span class="preview-center">
								<span class="hover-title"><?php the_title(); ?></span>
								<p><span class="hover-banner"><?php the_field('project_banner'); ?></span></p>
								<img src="/wp-content/themes/bfdg/images/overlay-arrow.svg" alt="View <?php the_title(); ?>">
							</span> <!-- .preview-center -->
						</a> <!-- .project-hover -->
					</div> <!-- .counter13 -->	

				</div> <!-- .home-row-big-left -->

			<?php elseif ($counter == 14): ?>
				<div class="home-row-small-right">

					<div class="counter14 project">
						<a href="<?php the_permalink(); ?>"><img src="<?php echo $home_small; ?>" alt="<?php echo $alt; ?>" class="preview"></a>
						<a href="<?php the_permalink(); ?>" class="project-hover">
							<span class="preview-center">
								<span class="hover-title"><?php the_title(); ?></span>
								<p><span class="hover-banner"><?php the_field('project_banner'); ?></span></p>
								<img src="/wp-content/themes/bfdg/images/overlay-arrow.svg" alt="View <?php the_title(); ?>">
							</span> <!-- .preview-center -->
						</a> <!-- .project-hover -->
					</div> <!-- .counter14 -->	

			<?php elseif ($counter == 15): ?>
			
					<div class="counter15 project">
						<a href="<?php the_permalink(); ?>"><img src="<?php echo $home_small; ?>" alt="<?php echo $alt; ?>" class="preview"></a>
						<a href="<?php the_permalink(); ?>" class="project-hover">
							<span class="preview-center">
								<span class="hover-title"><?php the_title(); ?></span>
								<p><span class="hover-banner"><?php the_field('project_banner'); ?></span></p>
								<img src="/wp-content/themes/bfdg/images/overlay-arrow.svg" alt="View <?php the_title(); ?>">
							</span> <!-- .preview-center -->
						</a> <!-- .project-hover -->
					</div> <!-- .counter15 -->

				</div> <!-- .home-row-small-right -->

			</div> <!-- .home-row -->

			<?php elseif ($counter == 16): ?>
			<!-- Row 6 -->
			<div class="home-row">

				<div class="home-row-small-left">

					<div class="counter16 project">
						<a href="<?php the_permalink(); ?>"><img src="<?php echo $home_small; ?>" alt="<?php echo $alt; ?>" class="preview"></a>
						<a href="<?php the_permalink(); ?>" class="project-hover">
							<span class="preview-center">
								<span class="hover-title"><?php the_title(); ?></span>
								<p><span class="hover-banner"><?php the_field('project_banner'); ?></span></p>
								<img src="/wp-content/themes/bfdg/images/overlay-arrow.svg" alt="View <?php the_title(); ?>">
							</span> <!-- .preview-center -->
						</a> <!-- .project-hover -->
					</div> <!-- .counter16 -->	

			<?php elseif ($counter == 17): ?>
			
					<div class="counter17 project">
						<a href="<?php the_permalink(); ?>"><img src="<?php echo $home_small; ?>" alt="<?php echo $alt; ?>" class="preview"></a>
						<a href="<?php the_permalink(); ?>" class="project-hover">
							<span class="preview-center">
								<span class="hover-title"><?php the_title(); ?></span>
								<p><span class="hover-banner"><?php the_field('project_banner'); ?></span></p>
								<img src="/wp-content/themes/bfdg/images/overlay-arrow.svg" alt="View <?php the_title(); ?>">
							</span> <!-- .preview-center -->
						</a> <!-- .project-hover -->
					</div> <!-- .counter17 -->	

				</div> <!-- .home-row-small-left -->

			<?php elseif ($counter == 18): ?>
				<div class="home-row-big-right">

					<div class="counter18 project">
						<a href="<?php the_permalink(); ?>"><img src="<?php echo $home_large; ?>" alt="<?php echo $alt; ?>" class="preview"></a>
						<a href="<?php the_permalink(); ?>" class="project-hover">
							<span class="preview-center">
								<span class="hover-title"><?php the_title(); ?></span>
								<p><span class="hover-banner"><?php the_field('project_banner'); ?></span></p>
								<img src="/wp-content/themes/bfdg/images/overlay-arrow.svg" alt="View <?php the_title(); ?>">
							</span> <!-- .preview-center -->
						</a> <!-- .project-hover -->
					</div> <!-- .counter18 -->	

				</div> <!-- .home-row-big-right -->
				
			</div> <!-- .home-row -->

			<?php //endif; // end view on home page conditional ?>


			<?php endif; // end counter conditional ?> 
			<?php endwhile; // end of the loop. ?>
		</div> <!-- .small-12 large-8 role=main -->
	</div> <!-- .row -->
</div> <!-- .wide -->
		
<?php get_footer(); ?>