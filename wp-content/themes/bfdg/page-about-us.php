<?php 
/* 
Template Name: About Us
*/
?>

<?php get_header(); ?>

	<div class="row">
		<div class="small-12 medium-12 collapse end" role="banner">
		
		<?php while (have_posts()) : the_post(); ?>
			<article <?php post_class() ?> id="post-<?php the_ID(); ?>">
				<div class="entry-content">
					<?php the_content(); ?>
				</div>
			</article>
		<?php endwhile;?>

		</div> <!-- .small-12 medium-12 role=banner -->
	</div> <!-- .row -->

<div class="wide">
	<div class="row">
		<div class="small-12 medium-12 columns" role="main">
			
			<div class="small-12 medium-12 columns">
				<section class="about-overview">
					<?php the_field('about_us_top_section'); ?>
				</section> 

				<aside class="about-process">
					<?php the_field('about_us_process'); ?>
				</aside>

				<section class="partial-client-list">
					<h2><?php the_field('client_list_heading') ?></h2>
				</section>  
			</div> <!-- .small-12 medium-12 -->
		</div> <!-- .small-12 medium-12 -->
	</div> <!-- .row -->

	<!-- Partial Client List -->
	<div class="row">
		<div class="small-12 medium-12 large-12 columns">

			<div class="small-12 medium-12 columns partial-client-list">
			<?php the_field('partial_client_list'); ?>
			</div> 
		</div> <!-- .small-12 medium-12 large-12 -->	
	</div> <!-- .row -->	


</div> <!-- .wide -->
		
<?php get_footer(); ?>