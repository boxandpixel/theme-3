<?php 
/* 
Template Name: Other Thoughts
*/
?>

<?php get_header(); ?>

	<div class="row">
		<div class="small-12 medium-12 collapse end" role="banner">
		
		<?php while (have_posts()) : the_post(); ?>
			<article <?php post_class() ?> id="post-<?php the_ID(); ?>">
				<div class="entry-content">
					<?php the_content(); ?>
				</div>
			</article>
		<?php endwhile;?>

		</div> <!-- .small-12 medium-12 role=banner -->
	</div> <!-- .row -->


<div class="wide">
	<div class="row">
		<div class="small-12 medium-12 columns" role="main">
			<div class="inrow">
			
		    <?php
		    	$args = array('post_type' => 'thoughts');  
		    	$loop = new WP_Query( $args );  
		    	while ( $loop->have_posts() ) : $loop->the_post();

				$image = get_field('thoughts_image');
				if( !empty($image) ): 
				$url = $image['url'];
				$alt = $image['alt'];
				endif; 		    	

			?>
				<div class="thoughts-item">
				    <a href="<?php the_permalink(); ?>"><img src="<?php echo $url; ?>" alt="<?php echo $alt; ?>"></a>
				    <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>

				</div> <!-- .thoughts-item -->
			<?php				
				
				//endif;
			?>

			<?php endwhile; // end of the loop. ?>

			</div> <!-- .row -->
		</div> <!-- .small-12 medium-12 -->
	</div> <!-- .row -->
</div> <!-- .wide -->
		
<?php get_footer(); ?>