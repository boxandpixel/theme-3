<?php 
/* 
Template Name: Error 404
*/
?>

<?php get_header(); ?>

	<div class="row">
		<div class="small-12 medium-12 collapse end" role="banner">
		
		<?php while (have_posts()) : the_post(); ?>
			<article <?php post_class() ?> id="post-<?php the_ID(); ?>">
				<div class="entry-content">
					<?php the_content(); ?>
				</div>
			</article>
		<?php endwhile;?>

		</div> <!-- .small-12 medium-12 role=banner -->
	</div> <!-- .row -->


<div class="wide">
	<div class="row">
		<div class="small-12 medium-12 columns" role="main">
			<div class="inrow">
				<div id="error-message">			
					<?php the_field('error_heading'); ?>
					<?php the_field('error_message'); ?>
					
				</div> <!-- #error-message -->
			</div> <!-- .inrow -->
		</div> <!-- .small-12 medium-12 -->
	</div> <!-- .row -->

</div> <!-- .wide -->
		
<?php get_footer(); ?>