"ontouchstart" in document.documentElement && ($(".project-hover").one("click", function() {
    return !1
}), $(".category-hover").one("click", function() {
    return !1
})), jQuery(window).scroll(function() {
    jQuery(window).scrollTop() > 200 ? jQuery("#back-to-top").fadeIn(200) : jQuery("#back-to-top").fadeOut(200)
}), jQuery("#back-to-top, .back-to-top").click(function() {
    return jQuery("html, body").animate({
        scrollTop: 0
    }, "800"), !1
}), $("#thoughts-prev").hover(function() {
    $(this).attr({
        src: "/wp-content/themes/bfdg/images/thoughts-prev-on.png"
    })
}, function() {
    $(this).attr({
        src: "/wp-content/themes/bfdg/images/thoughts-prev-off.png"
    })
}), $("#thoughts-menu").hover(function() {
    $(this).attr({
        src: "/wp-content/themes/bfdg/images/thoughts-menu-on.png"
    })
}, function() {
    $(this).attr({
        src: "/wp-content/themes/bfdg/images/thoughts-menu-off.png"
    })
}), $("#thoughts-next").hover(function() {
    $(this).attr({
        src: "/wp-content/themes/bfdg/images/thoughts-next-on.png"
    })
}, function() {
    $(this).attr({
        src: "/wp-content/themes/bfdg/images/thoughts-next-off.png"
    })
}), $(".share-icons li a.addthis_button_facebook img").hover(function() {
    $(this).attr({
        src: "/wp-content/themes/bfdg/images/social-facebook-on.png"
    })
}, function() {
    $(this).attr({
        src: "/wp-content/themes/bfdg/images/social-facebook.png"
    })
}), $(".share-icons li a.addthis_button_linkedin img").hover(function() {
    $(this).attr({
        src: "/wp-content/themes/bfdg/images/social-linkedin-on.png"
    })
}, function() {
    $(this).attr({
        src: "/wp-content/themes/bfdg/images/social-linkedin.png"
    })
}), $(".share-icons li a.addthis_button_twitter img").hover(function() {
    $(this).attr({
        src: "/wp-content/themes/bfdg/images/social-twitter-on.png"
    })
}, function() {
    $(this).attr({
        src: "/wp-content/themes/bfdg/images/social-twitter.png"
    })
}), $(".share-icons li a.addthis_button_google img").hover(function() {
    $(this).attr({
        src: "/wp-content/themes/bfdg/images/social-google-on.png"
    })
}, function() {
    $(this).attr({
        src: "/wp-content/themes/bfdg/images/social-google.png"
    })
}), $(".share-icons li a.addthis_button_email img").hover(function() {
    $(this).attr({
        src: "/wp-content/themes/bfdg/images/social-email-on.png"
    })
}, function() {
    $(this).attr({
        src: "/wp-content/themes/bfdg/images/social-email.png"
    })
}), $("#cat-wrap").hide(), $("#cat-overlay").hide();
var docHeight = $(document).height();
$("#cat-overlay").height(docHeight).css({
    opacity: .8,
    position: "fixed",
    top: 0,
    left: 0,
    "background-color": "#000",
    width: "100%",
    "z-index": 9996
}), $("#cat-overlay").on("click", function() {
    $(this).hide(), $("#cat-wrap").hide(), $(".plus").show()
});
var cat_links = $("#cat-nav ul li a");
$(".plus").click(function() {
    $("#cat-overlay").toggle(), $("#cat-wrap").toggle(), $(this).hide(), $("#cat-inside").load("/identity-and-branding", function() {
        "ontouchstart" in document.documentElement && $(".category-hover").one("click", function() {
            return !1
        })
    }), cat_links.removeClass("cat-curr"), $("#cat-identity-and-branding").addClass("cat-curr")
}), $("#cat-close").click(function() {
    $("#cat-overlay").hide(), $("#cat-wrap").hide(), $(".plus").show()
}), $("#expertise-identity-and-branding").click(function(t) {
    return $("#cat-overlay").toggle(), $("#cat-wrap").toggle(), $("#cat-inside").load("/identity-and-branding", function() {
        "ontouchstart" in document.documentElement && $(".category-hover").one("click", function() {
            return !1
        })
    }), cat_links.removeClass("cat-curr"), $("#cat-nav ul li a#cat-identity-and-branding").addClass("cat-curr"), t.preventDefault, !1
}), $("#cat-identity-and-branding").click(function(t) {
    $("#cat-inside").load("/identity-and-branding", function() {
        "ontouchstart" in document.documentElement && $(".category-hover").one("click", function() {
            return !1
        })
    }), cat_links.removeClass("cat-curr"), $(this).addClass("cat-curr"), t.preventDefault()
}), $("#expertise-interactive-media").click(function(t) {
    return $("#cat-overlay").toggle(), $("#cat-wrap").toggle(), $("#cat-inside").load("/interactive-media", function() {
        "ontouchstart" in document.documentElement && $(".category-hover").one("click", function() {
            return !1
        })
    }), cat_links.removeClass("cat-curr"), $("#cat-nav ul li a#cat-interactive-media").addClass("cat-curr"), !1;
    t.preventDefault
}), $("#cat-interactive-media").click(function(t) {
    $("#cat-inside").load("/interactive-media", function() {
        "ontouchstart" in document.documentElement && $(".category-hover").one("click", function() {
            return !1
        })
    }), cat_links.removeClass("cat-curr"), $(this).addClass("cat-curr"), t.preventDefault()
}), $("#expertise-marketing-communications").click(function(t) {
    return $("#cat-overlay").toggle(), $("#cat-wrap").toggle(), $("#cat-inside").load("/marketing-communications", function() {
        "ontouchstart" in document.documentElement && $(".category-hover").one("click", function() {
            return !1
        })
    }), cat_links.removeClass("cat-curr"), $("#cat-nav ul li a#cat-marketing-communications").addClass("cat-curr"), !1;
    t.preventDefault
}), $("#cat-marketing-communications").click(function(t) {
    $("#cat-inside").load("/marketing-communications", function() {
        "ontouchstart" in document.documentElement && $(".category-hover").one("click", function() {
            return !1
        })
    }), cat_links.removeClass("cat-curr"), $(this).addClass("cat-curr"), t.preventDefault()
}), $("#expertise-stakeholder-communications").click(function(t) {
    return $("#cat-overlay").toggle(), $("#cat-wrap").toggle(), $("#cat-inside").load("/stakeholder-communications", function() {
        "ontouchstart" in document.documentElement && $(".category-hover").one("click", function() {
            return !1
        })
    }), cat_links.removeClass("cat-curr"), $("#cat-nav ul li a#cat-stakeholder-communications").addClass("cat-curr"), t.preventDefault, !1
}), $("#cat-stakeholder-communications").click(function(t) {
    $("#cat-inside").load("/stakeholder-communications", function() {
        "ontouchstart" in document.documentElement && $(".category-hover").one("click", function() {
            return !1
        })
    }), cat_links.removeClass("cat-curr"), $(this).addClass("cat-curr"), t.preventDefault()
});

$(".menu-icon a").click(function() {
    $(".top-bar").toggleClass("expanded");
})