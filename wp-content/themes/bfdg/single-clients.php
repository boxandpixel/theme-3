<?php get_header(); ?>

	<div class="row">
		<div class="small-12 medium-12 collapse end" role="banner">
		
		<?php while (have_posts()) : the_post(); ?>
			<article <?php post_class() ?> id="post-<?php the_ID(); ?>">
				<div class="entry-content">
					<!-- <\?php the_field('project_banner'); ?> -->
				</div>
			</article>
		<?php endwhile;?>

		</div> <!-- .small-12 medium-12 role=banner -->
	</div> <!-- .row -->


<div class="wide">
	<div class="project-container" role="main">
	
	<?php while (have_posts()) : the_post(); ?>

		<div class="project-wide">
			<div class="row">
	    		<div class="small-12 medium-12 large-12 columns">
	    			<div class="inrow project-headings">
		        		<h1><?php the_field('h1_project_heading'); ?></h1>
		        		<h2><?php the_field('project_banner'); ?></h2>
	   				</div> <!-- .inrow -->
	    		</div> <!-- .small-12 medium-12 large-12 -->
	    	</div> <!-- .row -->
		</div> <!-- .project-wide -->	
	
	
	<?php 
		if( have_rows('project_content') ):

			while ( have_rows('project_content') ) : the_row();

		        if( get_row_layout() == 'project_description' ):
		        	?>
		        	<div class="project-wide">
		        		<div class="row">
			        		<div class="small-12 medium-12 large-12 columns">
			        			<div class="inrow">
					        		<div class="project-desc-<?php the_sub_field('style'); ?>">
			        					<?php the_sub_field('description'); ?>
			       					</div> <!-- .project-desc -->
			       				</div> <!-- .inrow -->
			        		</div> <!-- .small-12 medium-12 large-12 -->
			        	</div> <!-- .row -->
		        	</div> <!-- .project-wide -->
		        	<?php

		        elseif( get_row_layout() == 'project_image_single' ):
		        	?>
		        	<div class="project-wide">
		        		<div class="row">
			        		<div class="small-12 medium-12 large-12 columns">
			        			<div class="inrow">
				        		<div class="project-image-single">

		        	<?php
		        	$image = get_sub_field('image');
		        	$url = $image['url']; 
		        	$alt = $image['alt'];
		        	?>
						<img src="<?php echo $url; ?>" alt="<?php echo $alt; ?>">
		        	
		       					</div> <!-- .project-image-single -->
			       				</div> <!-- .inrow -->
			        		</div> <!-- .small-12 medium-12 large-12 -->
			        	</div> <!-- .row -->
		        	</div> <!-- .project-wide -->
		        	
				<?php

		        elseif( get_row_layout() == 'video' ):
		        	?>
		        	<div class="project-wide">
		        		<div class="row">
			        		<div class="small-12 medium-12 large-12 columns">
			        			<div class="inrow">
				        		<div class="project-image-single">

				        			<?php 
				        				$video = get_sub_field('video_url');

				        				if (!empty($video)):
				        					echo $video;
				        				else:
				        					//
				        				
				        				endif;
				        			?>
						        	<?php //get_sub_field('video_url'); ?>
		        	
		       					</div> <!-- .project-image-single -->
			       				</div> <!-- .inrow -->
			        		</div> <!-- .small-12 medium-12 large-12 -->
			        	</div> <!-- .row -->
		        	</div> <!-- .project-wide -->
		        	
				<?php				

				// Begin Gray Project Combo
		        elseif( get_row_layout() == 'project_combo_gray' ):
		        	?>
		        	<div class="project-wide project-gray">
		        		<div class="row">
			        		<div class="small-12 medium-12 large-12 columns">
			        			<div class="inrow">
				        		<div class="project-combo">
		        	<?php 
		        	if (get_sub_field('is_slideshow')):
		        		// This is a slideshow 
		        	?>		   
			        	<ul class="example-orbit">

			        	<?php
			        	if( have_rows('image')):
			        		while ( have_rows('image') ) : the_row();
			        			$image = get_sub_field('image');
			        			$url = $image['url']; 
					        	$alt = $image['alt'];
			        			
			        			?>			        		
			        		<li><img src="<?php echo $url; ?>" alt="<?php echo $alt; ?>"></li>
		        		<?php
		        			endwhile;
		        		endif;
		        		?>
			        	</ul>
			        <?php

		        	else:
		        		// Not a slideshow		   
			        	if( have_rows('image')):
			     		
			        		while ( have_rows('image') ) : the_row();
			        			$image = get_sub_field('image');
			        			$url = $image['url']; 
					        	$alt = $image['alt'];
			        			?>
			        			<img src="<?php echo $url; ?>" alt="<?php echo $alt; ?>">
			        			<?php
							endwhile;
						endif;		        	
		        	endif;
		        	?>


		        	<?php
					the_sub_field('description'); 
		        	?>
		       					</div> <!-- .project-combo -->
			       				</div> <!-- .inrow -->
			        		</div> <!-- .small-12 medium-12 large-12 -->
			        	</div> <!-- .row -->
		        	</div> <!-- .project-wide .project-gray -->
		        	<?php

				// Begin White Project Combo
		        elseif( get_row_layout() == 'project_combo_white' ):
		        	?>



		        	<div class="project-wide project-white">
		        		<div class="row">
			        		<div class="small-12 medium-12 large-12 columns">
			        			<div class="inrow">
				        		<div class="project-combo">
		        	<?php 
		        	if (get_sub_field('is_slideshow')):
		        		// This is a slideshow 
		        	?>		   
			        	<ul class="example-orbit">

			        	<?php
			        	if( have_rows('image')):
			        		while ( have_rows('image') ) : the_row();
			        			$image = get_sub_field('image');
			        			$url = $image['url']; 
					        	$alt = $image['alt'];
			        			?>			        		
			        		<li><img src="<?php echo $url; ?>" alt="<?php echo $alt; ?>"></li>
		        		<?php
		        			endwhile;
		        		endif;
		        		?>
			        	</ul>
			        <?php

		        	else:
		        		// Not a slideshow		   
			        	if( have_rows('image')):
			     		
			        		while ( have_rows('image') ) : the_row();
			        			$image = get_sub_field('image');
			        			$url = $image['url']; 
			        			$alt = $image['alt'];
			        			?>
			        			<img src="<?php echo $url; ?>" alt="<?php echo $alt; ?>">
			        			<?php
							endwhile;
						endif;		        	
		        	endif;
		        	?>


		        	<?php
					the_sub_field('description'); 
		        	?>
		       					</div> <!-- .project-combo -->
			       				</div> <!-- .inrow -->
			        		</div> <!-- .small-12 medium-12 large-12 -->
			        	</div> <!-- .row -->
		        	</div> <!-- .project-wide .project-white -->
		        	<?php		

				//end all
		        endif;             

 
    		endwhile;	
		endif;
	?>

	<?php 
		if( get_field('show_project_result') ) {
    ?>
    	<div class="row project-result-section">
    		<div class="small-12 medium-12 large-12 columns">
 				<section class="client-heading-result">
					<h2>The Result</h2>
	 			</section>
	 			<div class="inrow">
		 			<div class="project-result">
		 				<?php the_field('project_result'); ?>
		 			</div> <!-- .project-result -->
		 		</div> <!-- .inrow -->
    		</div> <!-- .small-12 medium-12 large-12 -->
    	</div> <!-- .row -->
  	
    <?php
    	} else {}
 	?>

	<?php 
		if( get_field('project_contact_form') ) {
    ?>
    	<div class="row client-form-intro">
    		<div class="small-12 medium-12 large-12 columns">
    			<div class="inrow">
    			<h2>To receive a complete case study or to see how we can help transition your brand, email us and we’ll get back to you asap.</h2>
			</div> <!-- .inrow -->
    		</div> <!-- .small-12 medium-12 large-12 -->
    	</div> <!-- .row -->

    	<div class="row client-form">
    		<div class="small-9 small-centered medium-9 medium-centered large-12 columns">
    			<div class="inrow">
    			<?php echo do_shortcode( '[contact-form-7 id="101" title="Client Detail Form"]' ); ?>
			</div> <!-- .inrow -->
    		</div> <!-- .small-12 medium-12 large-12 -->
    	</div> <!-- .row -->    	
    <?php
    	} else {}
 	?>

 		<div class="row client-share">
 			<div class="small-12 medium-12 large-12 columns">
 				<section class="client-heading-share">
					<h2>Share This Page</h2>
	 			</section>
				<!-- AddThis Button BEGIN -->
				<ul class="share-icons">
					<li><a class="addthis_button_facebook"><img src="/wp-content/themes/bfdg/images/social-facebook.png" alt=""></a></li>
					<li><a class="addthis_button_linkedin"><img src="/wp-content/themes/bfdg/images/social-linkedin.png" alt=""></a></li>
					<li><a class="addthis_button_twitter"><img src="/wp-content/themes/bfdg/images/social-twitter.png" alt=""></a></li>
					<li><a class="addthis_button_google"><img src="/wp-content/themes/bfdg/images/social-google.png" alt=""></a>
					<li><a class="addthis_button_email"><img src="/wp-content/themes/bfdg/images/social-email.png" alt=""></a></li>
				</ul>

				<script type="text/javascript">var addthis_config = {"data_track_addressbar":true};</script>
				<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5350317b110143e4"></script>
				<!-- AddThis Button END -->
 			</div> <!-- .small-12 medium-12 large-12 -->
 		</div> <!-- .row -->  	

 		<div class="row client-categories">
 			<div class="small-12 medium-12 large-12 columns">
 				<section class="client-heading">
					<h2>Expertise</h2>
	 			</section>
				<?php
					$args = array(
					  'orderby' => 'name',
					  'hide_empty' => 0, 
					  );
					$categories = get_categories( $args );
				?>
					<ul class="client-categories-list">
				<?php
					foreach ( $categories as $category ) {
						echo '<li><a id="expertise-' . $category->slug . '">' . $category->name . '</a></li>';
					}
				?>
					</ul> <!-- .client-cagegories -->
 			</div> <!-- .small-12 medium-12 large-12 -->
 		</div> <!-- .row --> 	


 		<div id="similar-projects-wrap">
 		<div class="row client-related">
 			<div class="small-12 medium-12 large-12 columns">
 				<div class="inrow">
 				<section class="client-heading-bottom">
					<h2>Similar Projects</h2>
	 			</section>
 				<?php 	
 					$posts = get_field('related_images');
if( $posts ): ?>
    <ul id="similar-projects">
    <?php foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>
        <?php setup_postdata($post); ?>
        <?php 
        	$image = get_field('project_image_related');
        	$url = $image['url'];
        ?>
        <li>
			<a href="<?php the_permalink(); ?>"><img src="<?php echo $url; ?>" alt="<?php echo $alt; ?>" class="preview"></a>
			<a href="<?php the_permalink(); ?>" class="category-hover">
				<span class="preview-center">
					<span class="hover-title"><?php the_title(); ?></span>
					<img src="/wp-content/themes/bfdg/images/overlay-arrow.svg" alt="View <?php the_title(); ?>">
				</span> <!-- .preview-center -->
			</a>      
        </li>
    <?php endforeach; ?>
    </ul>
    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
<?php endif; ?> 
			</div> <!-- .inrow -->
 			</div> <!-- .small-12 medium-12 large-12 -->
 		</div> <!-- .row --> 
	 	</div> <!-- #similar-projects-wrap -->
	
	<?php endwhile;?>


	</div> <!-- .project-container role=main -->
</div> <!-- .wide -->
		
<?php get_footer(); ?>