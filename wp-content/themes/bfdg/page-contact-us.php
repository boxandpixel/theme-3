<?php 
/* 
Template Name: Contact Us
*/
?>


<?php get_header(); ?>

<script type="application/ld+json">
	{
  	"@context": "http://schema.org",
  	"@type": "LocalBusiness",
	"additionalType": "http://www.productontology.org/doc/Branding_agency",
	"url": "https://bfdg.com",
	"logo": "http://www.example.com/images/logo.png",
	"email": "mailto:info@bfdg.com",
  	"address": {
    	"@type": "PostalAddress",
    	"addressLocality": "New York",
    	"addressRegion": "NY",
    	"postalCode":"10028",
    	"streetAddress": "55 East End Avenue, Suite 7K"
  	},
  	"description": "The Bernhardt Fudyma Design Group creates strategic branding and design solutions for companies in transition due to growth, mergers or market competition.",
  	"name": "Bernhardt Fudyma Design Group",
  	"image": "https://bfdg.com/wp-content/themes/bfdg/images/bernhardt-fudyma-search-logo.png",
  	"telephone": "212-889-9337",
  	"openingHours": "Mo,Tu,We,Th,Fr 09:00-17:00",
  	"geo": {
    	"@type": "GeoCoordinates",
   	"latitude": "40.7504304",
    	"longitude": "-76.1899276"
 		},
	"aggregateRating": {
	    "@type": "AggregateRating",
	    "ratingValue": "5",
	    "reviewCount": "2"
	 }, 			
  	"sameAs" : [ "https://www.facebook.com/BernhardtFudymaDesignGroup",
    	"https://www.linkedin.com/company/bernhardt-fudyma-design-group/"]
	}
</script>
	<div class="row">
		<div class="small-12 medium-12 collapse end" role="banner">
		
		<?php while (have_posts()) : the_post(); ?>
			<article <?php post_class() ?> id="post-<?php the_ID(); ?>">
				<div class="entry-content">
					<?php the_content(); ?>
				</div>
			</article>
		<?php endwhile;?>

		</div> <!-- .small-12 medium-12 role=banner -->
	</div> <!-- .row -->


<div class="wide">
	<div class="row">
		<div class="inrow">
		<div class="small-12 medium-12 columns" role="main">
			
			
			<div class="small-8 columns">
				<div class="inrow">
					

					<!-- <div class="contact-map"> -->
						<!-- New map -->
<!-- 						<div class="google-maps">
							<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3021.539490716163!2d-73.94892428456019!3d40.77215227932543!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c258b79f76742d%3A0x8d9f3a73b4c45a32!2s55+East+End+Ave%2C+New+York%2C+NY+10028!5e0!3m2!1sen!2sus!4v1511359315674" width="600" height="250" frameborder="0" style="border:0" allowfullscreen></iframe>
						</div> -->
					<!-- </div>  --><!-- .contact-map -->
					<div class="contact-form">
						<?php echo do_shortcode( '[contact-form-7 id="100" title="Contact Page Form"]' ); ?>
					</div> <!-- .contact-form -->
				</div> <!-- .inrow -->
			</div> <!-- .small-12 medium-8 -->

			<div class="small-4 columns">
				<section class="contact-address">
					<?php the_field('contact_address'); ?>
				</section>
				<section class="contact-phone">
					<?php the_field('contact_phone'); ?>
				</section>
				<section class="contact-fax">
					<?php the_field('contact_fax'); ?>
				</section>
			</div> <!-- .small-12 medium-4 -->
			
		</div> <!-- .small-12 medium-12 -->
		</div> <!-- .inrow -->
	</div> <!-- .row -->
</div> <!-- .wide -->
		
<?php get_footer(); ?>