<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "container" div.
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

?>
<!doctype html>
<html class="no-js" <?php language_attributes(); ?> >
	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<title><?php if ( is_category() ) {
		  echo 'Category Archive for &quot;'; single_cat_title(); echo '&quot; | '; bloginfo( 'name' );
		} elseif ( is_tag() ) {
		  echo 'Tag Archive for &quot;'; single_tag_title(); echo '&quot; | '; bloginfo( 'name' );
		} elseif ( is_archive() ) {
		  wp_title(''); echo ' Archive | '; bloginfo( 'name' );
		} elseif ( is_search() ) {
		  echo 'Search for &quot;'.esc_html($s).'&quot; | '; bloginfo( 'name' );
		} elseif ( is_home() || is_front_page() ) {
		  bloginfo( 'name' ); echo ' | '; bloginfo( 'description' );
		}  elseif ( is_404() ) {
		  echo 'Error 404 Not Found | '; bloginfo( 'name' );
		} elseif ( is_single() ) {
		  wp_title('');
		} else {
		  echo wp_title( ' | ', 'false', 'right' ); bloginfo( 'name' );
		} ?></title>
		
		
		<link rel="icon" href="<?php echo get_template_directory_uri(); ?>/dist/assets/images/favicon.ico" type="image/x-icon">
		<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo get_template_directory_uri(); ?>/dist/assets/images/apple-touch-icon-144x144-precomposed.png">
		<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo get_template_directory_uri(); ?>/dist/assets/images/apple-touch-icon-114x114-precomposed.png">
		<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo get_template_directory_uri(); ?>/dist/assets/images/apple-touch-icon-72x72-precomposed.png">
		<link rel="apple-touch-icon-precomposed" href="<?php echo get_template_directory_uri(); ?>/dist/assets/images/apple-touch-icon-precomposed.png">
		<link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:700,400,300,200,300italic,400italic' rel='stylesheet' type='text/css'>

		<!--[if IE 9]>
		  <style>
		    #cat-inside {
		      overflow-x: hidden !important;
		      box-sizing:content-box;
		    }

		    .share-icons li {
		      float: left;
		      margin: 0;
		    }

		    .client-form form input[type="submit"] {
		      background: url('/wp-content/themes/bfdg/images/form-submit.png') no-repeat scroll center 10px #AAAAA3;
		    }        

		  </style>
		<![endif]--> 		

		<style>
		    .google-maps {
		        position: relative;
		        padding-bottom: 65%; // This is the aspect ratio
		        height: 0;
		        overflow: hidden;
		    }
		    .google-maps iframe {
		        position: absolute;
		        top: 0;
		        left: 0;
		        width: 100% !important;
		        height: 100% !important;
		    }
		</style> 

		<?php wp_head(); ?>
	</head>
	<body <?php body_class(); ?>>

		<div id="header-wrap">
		  	<div id="cat-overlay"></div>

		  	<div class="row categories">
		    	<div class="small-12 medium-12 collapse">
		      
			        <div id="cat-wrap"> 
			          	<div id="cat-close">
			            	<img src="/wp-content/themes/bfdg/images/categories-close.svg" alt="">
			          	</div> <!-- #cat-close -->
			          	<div id="cat-nav">
			          <?php
			            $args = array(
			              'orderby' => 'name',
			              'hide_empty' => 0, 
			              );
			            $categories = get_categories( $args );
			          ?>
			            	<ul>
			          <?php
			            foreach ( $categories as $category ) {
			              echo '<li><a href="' . get_category_link( $category->term_id ) . '" id="cat-' . $category->slug . '">' . $category->name . '</a></li>';
			            }
			          ?>
			            	</ul> <!-- .drop-cagegories-list -->
			          	</div> <!-- #cat-nav -->

			          	<div id="cat-inside"></div> <!-- #cat-inside -->
			          
			        </div> <!-- #cat-wrap -->
		    	</div> <!-- .small-12 medium-12 -->
		  	</div> <!-- .row .categories -->

		  	<div class="row header">
		    	<div class="small-12 medium-12 large-12 collapse">
		        	<div class="top-bar-container contain-to-grid">
		            	<nav class="top-bar" data-topbar="">

		                	<ul class="title-area">
		                    	<li class="name">
		                        	<a href="<?php echo home_url(); ?>" id="logo"><?php bloginfo('name'); ?></a>
		                    	</li>
		                    	<li class="plus"></li>
		                    	<li class="toggle-topbar menu-icon">
		                    		<a href="#"></a>
		                    	</li>        
		                	</ul>
		                	<section class="top-bar-section">                    
		                    	<?php foundationPress_top_bar_r(); ?>
		                	</section>
		            	</nav>
		        	</div>
		    	</div> <!--  .small-12 -->
		    	<div class="small-12 medium-12 large-12 collapse">
		      		<section class="nav-top-social show-for-large-up">
		        		<?php foundationPress_top_nav_social(); ?>
		      		</section>
		    	</div> <!-- .small-12 -->
		  	</div> <!-- .row -->
		</div> <!-- #header-wrap -->
