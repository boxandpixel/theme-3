<?php 
/* 
Template Name: Interactive Media
*/
?>
<ul id="cat-thumbs">
<?php
	$args = array('post_type' => 'clients', 'category_name' => 'interactive-media');  
	$loop = new WP_Query( $args );  
	while ( $loop->have_posts() ) : $loop->the_post(); 

	$image = get_field('project_image_related');
	if( !empty($image) ): 
		$url = $image['url'];
		$alt = $image['alt'];
	endif;
?>

	<li>
		<a href="<?php the_permalink(); ?>"><img src="<?php echo $url; ?>" alt="<?php echo $alt; ?>" class="preview"></a>
		<a href="<?php the_permalink(); ?>" class="category-hover">
			<span class="preview-center">
				<span class="hover-title"><?php the_title(); ?></span>
				<img src="/wp-content/themes/bfdg/images/overlay-arrow.svg" alt="View <?php the_title(); ?>">
			</span> <!-- .preview-center -->
		</a>
	</li>
			
<?php endwhile; // end of the loop. ?>
	<li style="height: 166px;"><img src="/wp-content/themes/bfdg/images/cat-trans.png" height="166" alt=""></li>
	<li></li>

</ul> <!-- #cat-thumbs -->

<div id="cat-padding"></div>