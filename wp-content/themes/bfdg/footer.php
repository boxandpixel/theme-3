<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the "off-canvas-wrap" div and all content after.
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */
?>

<div id="footer-wrap">
    <div class="small-12 columns">
      <section class="nav-bottom-social hide-for-large-up">
        <?php foundationPress_top_nav_social(); ?>
      </section>
    </div> <!-- .small-12 -->	
	<footer class="row">
      <a href="/"><img src="/wp-content/themes/bfdg/images/bfdg-footer-logo.png" alt="Bernhardt Fudyma Design Group"></a>
      <?php dynamic_sidebar("Footer widgets"); ?>
      <p class="footer-bfdg">&copy; <?php echo date("Y"); ?> Bernhardt Fudyma Design Group</p>

  </footer>
</div> <!-- #footer-wrap -->

<div id="back-to-top">
  <a href="#">Back to top</a>
</div> <!--  #back-to-top -->

<?php wp_footer(); ?>


<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-20901871-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

<script type="text/javascript">
var addthis_config = addthis_config||{};
addthis_config.data_track_addressbar = false;
addthis_config.data_track_clickback = false;
</script>

</body>
</html>