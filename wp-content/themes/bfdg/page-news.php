<?php 
/* 
Template Name: News
*/
?>

<?php get_header(); ?>

	<div class="row">
		<div class="small-12 medium-12 collapse end" role="banner">
		
		<?php while (have_posts()) : the_post(); ?>
			<article <?php post_class() ?> id="post-<?php the_ID(); ?>">
				<div class="entry-content">
					<?php the_content(); ?>
				</div>
			</article>
		<?php endwhile;?>

		</div> <!-- .small-12 medium-12 role=banner -->
	</div> <!-- .row -->


<div class="wide">
	<div class="row">
		<div class="small-12 medium-12 columns" role="main">
			<div class="inrow">

			
		    <?php
		    	$args = array('post_type' => 'news');  
		    	$loop = new WP_Query( $args );  
		    	while ( $loop->have_posts() ) : $loop->the_post();

				$image = get_field('news_image');
				if( !empty($image) ): 
				$url = $image['url'];
				$alt = $image['alt'];
				endif; 		    	
			?>		     

			<div class="news-item">
			    <img src="<?php echo $url; ?>" alt="<?php echo $alt; ?>">
			    <div class="news-center">
				    <span class="date"><?php the_date('F Y'); ?></span> <h2><?php the_title(); ?></h2>
				    <?php the_content(); ?>	

				</div> <!-- .news-center -->
			</div> <!-- .news-item -->

			<?php endwhile; // end of the loop. ?>
			</div> <!-- .inrow -->
		</div> <!-- .small-12 medium-12 -->
	</div> <!-- .row -->
</div> <!-- .wide -->
		
<?php get_footer(); ?>